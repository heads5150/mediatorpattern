﻿using System;
using System.IO;
using System.Text;
using MediatorPattern.Core.Configuration;
using MediatrPattern.Console.Configuration;
using MediatR;
using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Shouldly;
using StructureMap;
using StructureMap.Graph;

namespace MediatorPattern.UnitTests.MediatorTests
{
    [TestFixture]
    public class Mediator
    {

        [Test]
        public void Can_Respond_To_Tests()
        {
            var c = new Container(cfg =>
            cfg.Scan(scanner =>
                {
                    scanner.TheCallingAssembly();
                    scanner.IncludeNamespaceContainingType<Ping>();
                    scanner.WithDefaultConventions();
                    scanner.AddAllTypesOf(typeof(IRequestHandler<,>));
                }));
            
            var serviceLocator = new StructureMapServiceLocator(c);
            var serviceLocatorProvider = new ServiceLocatorProvider(() => serviceLocator);
            c.Configure(cfg => cfg.For<ServiceLocatorProvider>().Use(serviceLocatorProvider));

            var mediator = new MediatR.Mediator(serviceLocatorProvider);

            var response = mediator.Send(new Ping {Message = "Ping"});

            response.Message.ShouldBe("Ping Pong");
        }

        [Test]
        public void Can_Respond_To_Publish()
        {
            var builder = new StringBuilder();
            var writer = new StringWriter(builder);

            var container = new Container(cfg =>
            {
                cfg.Scan(scanner =>
                {
                    scanner.TheCallingAssembly();
                    scanner.IncludeNamespaceContainingType<Ping>();
                    scanner.WithDefaultConventions();
                    scanner.AddAllTypesOf(typeof(INotificationHandler<>));
                });
                cfg.For<TextWriter>().Use(writer);
            });

            var serviceLocator = new StructureMapServiceLocator(container);
            var serviceLocatorProvider = new ServiceLocatorProvider(() => serviceLocator);
            container.Configure(cfg => cfg.For<ServiceLocatorProvider>().Use(serviceLocatorProvider));

            var mediator = new MediatR.Mediator(serviceLocatorProvider);

            mediator.Publish(new PingNotification{Message="Ping"});

            var result = builder.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            result.ShouldContain("Ping Pong");
        }
    }

    public class Ping : IRequest<Pong>
    {
        public string Message { get; set; }
    }    
    
    public class PingNotification : INotification
    {
        public string Message { get; set; }
    }

    public class Pong
    {
        public string Message { get; set; }
    }

    public class PingHandler : IRequestHandler<Ping, Pong>
    {
        public Pong Handle(Ping message)
        {
            return new Pong { Message = message.Message + " Pong" };
        }
    }

    public class PingNotificationHandler : INotificationHandler<PingNotification>
    {
          private readonly TextWriter writer;

          public PingNotificationHandler(TextWriter writer)
            {
                this.writer = writer;
            }

        public void Handle(PingNotification notification)
        {
            writer.WriteLine(notification.Message + " Pong");
        }
    }

    
}