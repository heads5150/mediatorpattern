﻿using System;
using System.Threading;
using MediatorPattern.Core.Configuration;
using MediatorPattern.Core.Data;
using MediatrPattern.Console.Configuration;
using MediatR;
using NUnit.Framework;
using Shouldly;
using StructureMap;

namespace MediatorPattern.UnitTests.ConfigurationTests
{
    [TestFixture]
    public class StructureMap
    {
        [Test, Ignore]
        public void CoreRegistry_What_Does_It_Have()
        {
            var container = Ioc.InitIoC();

            Console.WriteLine(container.WhatDoIHave());
        }

        [Test]
        public void Builds_IMediator()
        {
            var container = Ioc.InitIoC();

            var mediator = container.GetInstance<IMediator>();

            mediator.ShouldNotBe(null);
        }
        
        [Test]
        public void Builds_MediatorPatternContext()
        {
            var container = Ioc.InitIoC();

            var mediator = container.GetInstance<MediatorPatternContext>();

            mediator.ShouldNotBe(null);
        }
    }
}