﻿using MediatR;

namespace MediatorPattern.Core.Configuration
{
    public class MediatorPipeline<TRequest, TResponse>
    : IRequestHandler<TRequest, TResponse>
    where TRequest : IRequest<TResponse>
    {

        private readonly IRequestHandler<TRequest, TResponse> inner;
        private readonly IPreRequestHandler<TRequest>[] preRequestHandlers;
        private readonly IPostRequestHandler<TRequest, TResponse>[] postRequestHandlers;

        public MediatorPipeline(
            IRequestHandler<TRequest, TResponse> inner,
            IPreRequestHandler<TRequest>[] preRequestHandlers,
            IPostRequestHandler<TRequest, TResponse>[] postRequestHandlers
            )
        {
            this.inner = inner;
            this.preRequestHandlers = preRequestHandlers;
            this.postRequestHandlers = postRequestHandlers;
        }

        public TResponse Handle(TRequest message)
        {

            foreach (var preRequestHandler in preRequestHandlers)
            {
                preRequestHandler.Handle(message);
            }

            var result = inner.Handle(message);

            foreach (var postRequestHandler in postRequestHandlers)
            {
                postRequestHandler.Handle(message, result);
            }

            return result;
        }
    }

    public interface IPreRequestHandler<in TRequest>
    {
        void Handle(TRequest request);
    }

    public interface IPostRequestHandler<in TRequest, in TResponse>
    {
        void Handle(TRequest request, TResponse response);
    }
}