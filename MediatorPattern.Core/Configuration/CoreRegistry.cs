﻿using System;
using System.Collections.Generic;
using System.Linq;
using MediatorPattern.Core.Data;
using MediatR;
using Microsoft.Practices.ServiceLocation;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;

namespace MediatorPattern.Core.Configuration
{
    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.AssemblyContainingType<IMediator>();
                s.WithDefaultConventions();
                s.AddAllTypesOf(typeof(IRequestHandler<,>));
                s.AddAllTypesOf(typeof(IAsyncRequestHandler<,>));
                s.AddAllTypesOf(typeof(INotificationHandler<>));
                s.AddAllTypesOf(typeof(IAsyncNotificationHandler<>));
               
            });
                
            For<MediatorPatternContext>().Use(new MediatorPatternContext());
            For(typeof (IRequestHandler<,>)).DecorateAllWith(typeof(MediatorPipeline<,>));

        }
    }


    public class StructureMapServiceLocator : ServiceLocatorImplBase
    {
        private readonly IContainer _container;

        public StructureMapServiceLocator(IContainer container)
        {
            _container = container;
        }

        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType).Cast<object>();
        }

        protected override object DoGetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return serviceType.IsAbstract || serviceType.IsInterface
                           ? _container.TryGetInstance(serviceType)
                           : _container.GetInstance(serviceType);
            }

            return _container.GetInstance(serviceType, key);
        }
    }
}