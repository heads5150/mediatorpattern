﻿using System.Linq;
using FluentValidation;
using MediatR;

namespace MediatorPattern.Core.Validation
{
    public class ValidationRequestHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IRequestHandler<TRequest, TResponse> innerHander;
        private readonly IValidator<TRequest>[] validators;

        public ValidationRequestHandler(IRequestHandler<TRequest, TResponse> innerHandler, IValidator<TRequest>[] validators)
        {
            this.validators = validators;
            innerHander = innerHandler;
        }

        public TResponse Handle(TRequest message)
        {
            var context = new ValidationContext(message);

            var failures =
                validators.Select(v => v.Validate(context)).SelectMany(r => r.Errors).Where(f => f != null).ToList();

            if (failures.Any())
            {
                throw new ValidationException(failures);
            }

            return innerHander.Handle(message);
        }
    }
}