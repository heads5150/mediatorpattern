﻿using MediatorPattern.Core.Configuration;
using Microsoft.Practices.ServiceLocation;
using StructureMap;
using StructureMap.Graph;

namespace MediatrPattern.Console.Configuration
{
    public static class Ioc
    {
        public static Container InitIoC()
        {
            var container = new Container(x =>
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.AssemblyContainingType<CoreRegistry>();
                    scan.WithDefaultConventions();
                    scan.LookForRegistries();
                }));


            //set up common service locator
            var serviceLocator = new StructureMapServiceLocator(container);
            var serviceLocatorProvider = new ServiceLocatorProvider(() => serviceLocator);
            container.Configure(cfg =>
            {
                cfg.For<ServiceLocatorProvider>().Use(serviceLocatorProvider);
            });

            return container;
        } 
    }
}